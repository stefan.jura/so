#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <limits.h>
#include <sys/types.h>
#include <stdarg.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <ctype.h>
#include <dirent.h>
#include <string.h>

#define SNAPSHOTS "SNAPSHOTS"
#define ISO "ISO"
#define PATH_SCRIPT "/Users/stefanjura/Desktop/STEFAN/dangerous.sh"
#define PATH_ISO "/Users/stefanjura/Desktop/STEFAN/ISO/"
#define MAX 200

typedef struct {
    ino_t st_ino;
    char filename[50];
    mode_t mode;
    off_t size;
    time_t modification;
    time_t access;
    char perm[10];
} myStruct;

myStruct curr, prev;
struct stat file_stat;

void writeSS(const char *snapshot_path, myStruct data) 
{
    int snapshot = open(snapshot_path, O_WRONLY | O_CREAT | O_TRUNC, 0666);
    if (snapshot == -1) 
    {
        perror("Error opening snapshot");
        return;
    }
    if (write(snapshot, &data, sizeof(myStruct)) == -1) 
    {
        perror("Error writing snapshot");
    }
    close(snapshot);
}

int readSS(const char *snapshot_path, myStruct *data) 
{
    int snapshot = open(snapshot_path, O_RDONLY);
    if (snapshot == -1) {
        perror("Error opening snapshot");
        return 0;
    }
    if (read(snapshot, data, sizeof(myStruct)) == -1) 
    {
        perror("Error reading snapshot");
        close(snapshot);
        return 0;
    }
    close(snapshot);
    return 1;
}

char *permissionsString(mode_t mode) 
{
    static char symbolic[11];
    strcpy(symbolic, "---------\0");
    for (int i = 0; i < 3; ++i) 
    {
        if (mode & (S_IRUSR >> (i * 3))) symbolic[i * 3] = 'r';
        if (mode & (S_IWUSR >> (i * 3))) symbolic[i * 3 + 1] = 'w';
        if (mode & (S_IXUSR >> (i * 3))) symbolic[i * 3 + 2] = 'x';
    }
    return symbolic;
}

void compare(const char *path, FILE *f, myStruct curr, myStruct prev) 
{
    int changes = 0;
    if (curr.access != prev.access) 
    {
        fprintf(f, "File: %s was accessed\n", curr.filename);
        changes = 1;
    }
    if (curr.modification != prev.modification) 
    {
        fprintf(f, "File: %s was modified\n", curr.filename);
        changes = 1;
    }
    if (curr.size != prev.size) 
    {
        fprintf(f, "File: %s size changed from %lld to %lld\n", curr.filename, prev.size, curr.size);
        changes = 1;
    }
    if (curr.mode != prev.mode) 
    {
        fprintf(f, "File: %s permissions changed from %s to %s\n", curr.filename, permissionsString(prev.mode), permissionsString(curr.mode));
        changes = 1;
    }
    if (!changes) 
    {
        fprintf(f, "No change for file: %s\n", curr.filename);
    }
    fprintf(f, "\n");
}

void verifyDangerous(const char *perm, const char *path) 
{
    if (strcmp(perm, "---------") == 0) 
    {
        int pipefd[2];
        if (pipe(pipefd) == -1) 
        {
            perror("pipe error");
            return;
        }

        pid_t pid = fork(); 
        if (pid == -1) 
        {
            perror("fork error");
            return;
        }

        if (pid == 0) 
        {
            close(pipefd[0]);


            if (dup2(pipefd[1], STDOUT_FILENO) == -1) 
            {
                perror("dup2 error");
                exit(EXIT_FAILURE);
            }
            close(pipefd[1]);


            execl(PATH_SCRIPT, "dangerous.sh", path, PATH_ISO, NULL);
            perror("execl failed");
            exit(EXIT_FAILURE);
        } 
        else 
        {
            close(pipefd[1]);

            char aux[1024];
            ssize_t a;

            while ((a = read(pipefd[0], aux, sizeof(aux) - 1)) > 0) 
            {
                aux[a] = '\0';
                printf("%s\n", aux);
            }

            wait(NULL);
            close(pipefd[0]);
        }
    }
}

void checkRecursive(const char *directory, FILE *f) 
{
    DIR *dir = opendir(directory);
    if (!dir) 
    {
        perror("Failed to open directory");
        return;
    }
    struct dirent *entry;
    while ((entry = readdir(dir))) 
    {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;
        char path[MAX * 2];
        snprintf(path, sizeof(path), "%s/%s", directory, entry->d_name);
        if (stat(path, &file_stat) != 0)
         {
            perror("Failed to get file stats");
            continue;
        }
        myStruct curr = {.st_ino = file_stat.st_ino, .mode = file_stat.st_mode, .size = file_stat.st_size, .modification = file_stat.st_mtime, .access = file_stat.st_atime};
        strncpy(curr.filename, entry->d_name, sizeof(curr.filename) - 1);
        strcpy(curr.perm, permissionsString(curr.mode));
        char snapshot_path[MAX * 2];
        snprintf(snapshot_path, sizeof(snapshot_path), "%s/%s.SNAPSHOT", SNAPSHOTS, entry->d_name);
        myStruct prev;
        int snapshotExists = readSS(snapshot_path, &prev);

        verifyDangerous(curr.perm, path);

        if (!snapshotExists) 
        {
            writeSS(snapshot_path, curr);
        }
        else
        {
            compare(path, f, curr, prev);
            writeSS(snapshot_path, curr);
        }

        if (S_ISDIR(file_stat.st_mode)) 
        {
            checkRecursive(path, f);
        }
    }
    closedir(dir);
}


void process_directory(const char *directory, FILE *f)
{
    printf("Processing directory: %s\n", directory);
    checkRecursive(directory, f);
}

int main(int argc, char *argv[])
{
    if (argc < 6)
    {
        perror("wrong arguments\n");
        exit(EXIT_FAILURE);
    }

    FILE *f = fopen("cmp.txt", "w");
    if (!f) 
    {
        perror("Failed to open file");
        exit(-1);
    }

    const char *snapshots = argv[2];
    const char *isolated = argv[4];

    int child_count = 0;
    for (int i = 5; i < argc; i++)
    {
        const char *input_dir = argv[i];
        pid_t pid = fork();
        if (pid == -1)
        {
            perror("fork");
            exit(EXIT_FAILURE);
        }
        else if (pid == 0)
        {
            process_directory(input_dir, f);
            exit(EXIT_SUCCESS);
        }
        else
        {
            child_count++;
        }
    }

    int status;
    pid_t child_pid;
    for (int i = 0; i < child_count; i++)
    {
        child_pid = wait(&status);
        if (WIFEXITED(status))
        {
            printf("Child Process %d terminated with PID %d and exit code %d.\n", i + 1, child_pid, WEXITSTATUS(status));
        }
        else
        {
            printf("Child Process %d terminated with PID %d and errors.\n", i + 1, child_pid);
        }
    }

    fclose(f);

    return 0;
}
