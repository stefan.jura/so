#include <fstream>
using namespace std;
ifstream fin("graf.in");
ofstream fout("graf.out");
const int inf=1000000000;
int n,m,A[101][101],dmin=100,S[101];

void RF()
{
    for(int k=1;k<=n;k++)
        for(int i=1;i<=n;i++)
            for(int j=1;j<=n;j++)
                if(A[i][j]>A[i][k]+A[k][j])
                    A[i][j]=A[i][k]+A[k][j];
}

int main()
{
    fin>>n>>m;
    for(int i=1;i<=n;i++)
        for(int j=1;j<=n;j++)
            if(i!=j) A[i][j]=inf;
    for(int i=1;i<=m;i++)
    {
        int x,y;
        fin>>x>>y;
        A[x][y]=1;
    }
    RF();
    for(int i=1;i<=n;i++)
    {
        int s=0;
        for(int j=1;j<=n;j++)
            s=s+A[i][j];
        S[i]=s;
        if(S[i]<dmin) dmin=S[i];
    }
    for(int i=1;i<=n;i++)
        if(S[i]==dmin)
            fout<<i<<" ";
    return 0;
}
