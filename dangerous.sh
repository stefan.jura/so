#!/bin/bash

file_path=$1
iso_dir=$2

chmod 777 "$file_path"

lines=$(awk 'END {print NR}' "$file_path")
words=$(awk '{total += NF} END {print total}' "$file_path")
chars=$(awk '{total += length} END {print total}' "$file_path")


echo "Lines: $lines, Words: $words, Characters: $chars"

if [ "$lines" -lt 3 ] && [ "$words" -gt 1000 ] && [ "$chars" -gt 2000 ]; then
    if grep -q -E 'corrupted|dangerous|risk|attack|malware|malicious' "$file_path" || LC_ALL=C grep -q '[^[:print:][:space:]]' "$file_path"; then
        echo "$(basename "$file_path") - DANGEROUS"
        mv "$file_path" "$iso_dir"
    else
        echo "$(basename "$file_path") - SAFE"
    fi
else
    echo "$(basename "$file_path") - SAFE"
fi
